
### What is this repository for? ###
This project was my AI project, we were team of 3 working on it. The main target of this project, is using search algorithms like BFS to solve a maze in the minimum number of steps.
We implemented Prim's Algorithm to construct the maze.


### How do I get set up? ###

Just run main.py file, and it will generate the maze and solve it.
